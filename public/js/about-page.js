class AboutPage extends HTMLElement {
  static #template = document.createElement('template')
  static {
    this.#template.innerHTML = `
      <h1>About client.pub</h1>
      <p>
        This is a simple ActivityPub client. It allows you to follow users and
        see their posts.
      </p>
      <p>
        <a href="#/">Back to home</a>
      </p>
      `
  }
  constructor() {
    super();
    this.attachShadow({ mode: 'open' });
    this.shadowRoot.appendChild(AboutPage.#template.content.cloneNode(true));
  }
}

customElements.define('about-page', AboutPage)
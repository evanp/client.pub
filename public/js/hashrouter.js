class HashRouter {

  #root = null

  constructor(routes, notFoundComponent = 'not-found') {
      this.routes = this.parseRoutes(routes);
      this.notFoundComponent = notFoundComponent;
  }

  mount(root) {
      this.#root = root
      window.addEventListener('hashchange', () => this.route(), false);
      window.addEventListener('load', () => this.route(), false);
  }

  parseRoutes(routes) {
      // Convert route definitions into a format that's easier to match against hashes
      return Object.keys(routes).map(route => ({
          regex: new RegExp('^' + route.replace(/:[^\s/]+/g, '([^\\s/]+)') + '$'),
          component: routes[route],
          params: route.match(/:([^\s/]+)/g)
      }));
  }

  route() {
      const hash = window.location.hash.replace('#', '') || '/';
      for (const route of this.routes) {
          const match = hash.match(route.regex);
          if (match) {
              const params = {};
              if (route.params) {
                  route.params.forEach((param, index) => {
                      // Remove the colon prefix and assign the matched values
                      params[param.substring(1)] = match[index + 1];
                  });
              }
              this.loadComponent(route.component, params);
              return;
          }
      }
      // If no routes matched, load the notfound component
      this.loadComponent(this.notFoundComponent, {});
  }

  loadComponent(componentName, params) {
    if (this.#root) {
      // Assuming the component takes a single object with parameters
      // You may need to adjust this depending on your component setup
     this.#root.innerHTML = `<${componentName} ${Object.entries(params).map(([key, value]) => `${key}="${value}"`).join(' ')}></${componentName}>`;
    }
  }
}
class LandingPage extends HTMLElement {
  static #template = document.createElement('template')
  static {
    this.#template.innerHTML = `
      <h1>Welcome to client.pub</h1>
      <p>
        This is a simple ActivityPub client. It allows you to follow users and
        see their posts.
      </p>
      <p>
        <a href="#/about">About</a>
      </p>
      `
  }
  constructor() {
    super();
    this.attachShadow({ mode: 'open' });
    this.shadowRoot.appendChild(LandingPage.#template.content.cloneNode(true));
  }
}

customElements.define('landing-page', LandingPage)